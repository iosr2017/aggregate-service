package pl.edu.agh.iosr;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.palantir.docker.compose.connection.waiting.HealthChecks;
import org.json.JSONException;
import org.junit.ClassRule;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.PathVariable;
import pl.edu.agh.iosr.client.CrawlerClient;
import pl.edu.agh.iosr.model.*;
import com.palantir.docker.compose.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by user on 13.12.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Category(IntegrationTest.class)
public class AggregateServiceTests {


//    @ClassRule
//    public static DockerComposeRule docker = DockerComposeRule.builder()
//            .file("src/test/resources/docker-compose.yml")
//            .waitingForService("aggregate", HealthChecks.toHaveAllPortsOpen())
//            .waitingForService("lubimy-czytac", HealthChecks.toHaveAllPortsOpen())
//            .waitingForService("crawler", HealthChecks.toHaveAllPortsOpen())
//            .waitingForService("aggregate", HealthChecks.toRespondOverHttp(11000, (port) -> port.inFormat("https://$HOST:$EXTERNAL_PORT")))
//            .waitingForService("lubimy-czytac", HealthChecks.toRespondOverHttp(10000, (port) -> port.inFormat("https://$HOST:$EXTERNAL_PORT")))
//            .waitingForService("crawler", HealthChecks.toRespondOverHttp(9000, (port) -> port.inFormat("https://$HOST:$EXTERNAL_PORT")))
//            .saveLogsTo("build/dockerLogs/dockerComposeRuleTest")
//            .build();
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    

    private static final Logger LOG = LoggerFactory.getLogger(AggregateServiceTests.class);
    @Autowired
    private CrawlerClient crawlerClient;


    public Data createTestDataEntry(){
        Author expAuthor = new Author();
        expAuthor.setUrl("http://lubimyczytac.pl/autor/6520/adam-mickiewicz");
        expAuthor.setFullName("Adam Mickiewicz");
        Data expected = new Data();
        expected.setTitle("test");
        expected.setAuthor(expAuthor);
        expected.setCover("http://s.lubimyczytac.pl/upload/default-book-50x75.jpg");
        expected.setRating(0.0);
        expected.setUrl("http://lubimyczytac.pl/ksiazka/4811446/test");
        return expected;
    }

    public Book createTestBookEntry(){
        Data data = new Data();
        SimpleBook simpleBook = new SimpleBook();
        simpleBook.setTitle("Velika borot'ba : v časi christìjans'kich vìkìv");
        simpleBook.setPublisher("\"Poselstwo Wyzwolenia\",");
        simpleBook.setPublicationYear("1990");
        Book book = new Book(data,simpleBook);
        return book;
    }

// integration with lubimy-czytac service
    @Test
    public void testGetBasicDataResponse() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBasicData/testytest", HttpMethod.GET, entity, String.class);
        assertEquals(response.getStatusCodeValue(), 200);
    }


    @Test
    public void testGetBasicDataNoRecord() throws JsonProcessingException, JSONException {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBasicData/testytest", HttpMethod.GET, entity, String.class);
        Data expected = new Data();
        JSONAssert.assertEquals(mapper.writeValueAsString(expected), response.getBody(), false);
    }

    @Test
    public void testServiceContent() throws JsonProcessingException, JSONException {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBasicData/test", HttpMethod.GET, entity, String.class);
        Data expected = createTestDataEntry();
        JSONAssert.assertEquals(mapper.writeValueAsString(expected), response.getBody(), false);
    }

    @Test
    public void testGetBasicDataPost() throws JsonProcessingException, JSONException {
        String body = "{\"title\":\"test\"}";
        HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(body, jsonHeaders);
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBasicData", HttpMethod.POST, entity, String.class);
        Data expected = createTestDataEntry();
        LOG.info(response.getBody());
        JSONAssert.assertEquals(mapper.writeValueAsString(expected), response.getBody(), false);
    }

    @Test
    public void testGetBasicDataBatchCount() throws IOException {
        int expected = 10;
        String body = "[";
        for(int i =1; i<expected; i++){
            body+="{\"title\":\""+i+"\"},";
        }
        body+="{\"title\":\"test\"}]";

        HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(body, jsonHeaders);
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBasicDataBatch", HttpMethod.POST, entity, String.class);
        ArrayList<Data> list = mapper.readValue(response.getBody(),new TypeReference<ArrayList<Data>>(){});
        assertEquals(list.size(),expected);


    }

    // integration with crawler service

    @Test
    public void testCrawlerClientResponse() throws JsonProcessingException, JSONException {

        BookPortion response = crawlerClient.getBooksFromYear("996","0");
        ObjectMapper mapper = new ObjectMapper();
        BookPortion expected = new BookPortion();
        expected.setNextId("END");
        expected.setBookList(new ArrayList<SimpleBook>());
        JSONAssert.assertEquals(mapper.writeValueAsString(expected),mapper.writeValueAsString(response), false);

    }

    @Test
    public void testCrawlerClientSingleRequestItemCount() {

        BookPortion response = crawlerClient.getBooksFromYear("1990","0");
        assertEquals(response.getBookList().size(),100);

    }

    @Test
    public void testCrawlerClientPortionCount() {
        int expected = 39;
        int counter = 0;
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ObjectMapper mapper = new ObjectMapper();
        String id = "0";
        while (!id.equals("END")) {
            BookPortion response = crawlerClient.getBooksFromYear("1870",id);
            id = response.getNextId();
            counter++;
        }
        assertEquals(counter, expected);
    }

    // integration with crawler & lubimy-czytac services
    @Test
    public void testGetBooksFromYearEmpty() throws JsonProcessingException, JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBooksFromYear/996/1", HttpMethod.GET, entity, String.class);
        List<Book> expected = new ArrayList<>();
        JSONAssert.assertEquals(mapper.writeValueAsString(expected), response.getBody(), false);

    }

    @Test
    public void testGetBooksFromYearContent() throws IOException, JSONException {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBooksFromYear/1990/1", HttpMethod.GET, entity, String.class);
        List<Book> received = mapper.readValue(response.getBody(), new TypeReference<ArrayList<Book>>() {});
        Book expected = createTestBookEntry();
         JSONAssert.assertEquals(mapper.writeValueAsString(expected) ,mapper.writeValueAsString(received.get(0)), false);

    }

    @Test
    public void testGetBooksFromYearCount() throws IOException, JSONException {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ObjectMapper mapper = new ObjectMapper();
        for(int i =1; i<=5; i++) {
            ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBooksFromYear/1990/1", HttpMethod.GET, entity, String.class);
            List<Book> received = mapper.readValue(response.getBody(), new TypeReference<ArrayList<Book>>() {
            });
            assertEquals(received.size(), 100);
        }
    }

    @Test
    public void testGetBooksAllFromYearEmpty() throws JsonProcessingException, JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ObjectMapper mapper = new ObjectMapper();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:11000/getBooksAllFromYear/996", HttpMethod.GET, entity, String.class);
        List<Book> expected = new ArrayList<>();
        JSONAssert.assertEquals(mapper.writeValueAsString(expected), response.getBody(), false);

    }



}


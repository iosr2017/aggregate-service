package pl.edu.agh.iosr.model;

import java.util.ArrayList;

/**
 * Created by user on 10.12.2017.
 */
public class BookPortion {

    private String nextId;
    private ArrayList<SimpleBook> bookList;

    public String getNextId() {
        return nextId;
    }

    public void setNextId(String nextId) {
        this.nextId = nextId;
    }

    public ArrayList<SimpleBook> getBookList() {
        return bookList;
    }

    public void setBookList(ArrayList<SimpleBook> bookList) {
        this.bookList = bookList;
    }

}

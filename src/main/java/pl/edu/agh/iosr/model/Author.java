package pl.edu.agh.iosr.model;

import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

public class Author {
    String url;
    String fullName;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


}

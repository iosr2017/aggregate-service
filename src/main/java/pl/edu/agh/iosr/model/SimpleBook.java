package pl.edu.agh.iosr.model;


/**
 * Created by user on 07.12.2017.
 */

public class SimpleBook {


    private String title;


    private String author;


    private String publisher;

    private String publicationYear;

    private boolean available = true;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(String year) {
        this.publicationYear = year;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString(){
        return "{title: " + title +"; author: "+author+"; publisher: "+publisher+";publicationYear: "+publicationYear+"}";
    }
}

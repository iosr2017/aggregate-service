package pl.edu.agh.iosr.model;

import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

public class Data {
    String url;
    String title;
    Author author;
    String cover;
    String category;
    Double rating;

    public Data(String title) {
        this.title = title;
    }

    public Data() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Data{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", author=" + author +
                ", cover='" + cover + '\'' +
                ", category='" + category + '\'' +
                ", rating=" + rating +
                '}';
    }
}

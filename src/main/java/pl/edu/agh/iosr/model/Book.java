package pl.edu.agh.iosr.model;

public class Book extends Data{

    private String publisher;

    private String year;

    private boolean available = true;

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Book(){
    }

    public Book(Data data, SimpleBook simpleBook) {
        this.setAvailable(simpleBook.isAvailable());
        this.setPublisher(simpleBook.getPublisher());
        this.setYear(simpleBook.getPublicationYear());
        this.setAuthor(data.getAuthor());
        this.setCategory(data.getCategory());
        this.setCover(data.getCover());
        this.setRating(data.getRating());
        if(data.getTitle() == null)
            this.setTitle(simpleBook.getTitle());
        else
            this.setTitle(data.getTitle());
        this.setUrl(data.getUrl());
    }
}

package pl.edu.agh.iosr.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import pl.edu.agh.iosr.client.LubimyCzytacClient;
import pl.edu.agh.iosr.model.Book;
import pl.edu.agh.iosr.model.Data;
import pl.edu.agh.iosr.model.SimpleBook;
import pl.edu.agh.iosr.model.Title;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class AsyncServiceImpl implements AsyncService {
    @Autowired
    private LubimyCzytacClient lubimyCzytacClient;

    private static final Logger LOG = LoggerFactory.getLogger(AsyncService.class);
    @Override
    @Async
    public CompletableFuture<List<Book>> getBooks(List<SimpleBook> list) throws InterruptedException {
        LOG.info("start Async task on: " + list);
        List<Book> books = new ArrayList<>();
        List<Title> titles = new ArrayList<>();
        list.forEach(e->{
            String title = e.getTitle();
            title =title.split("\\s\\s+")[0];
            e.setTitle(title);
            titles.add(new Title(title));
        });
        List<Data> data = lubimyCzytacClient.getBasicDataBatch(titles);
        Iterator<SimpleBook> iterator = list.iterator();
        data.forEach(d->{
            books.add(new Book(d, iterator.next()));
        });
        LOG.info("end Async task on: " + list);
        return CompletableFuture.completedFuture(books);
    }
}

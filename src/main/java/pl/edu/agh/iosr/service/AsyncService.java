package pl.edu.agh.iosr.service;

import pl.edu.agh.iosr.model.Book;
import pl.edu.agh.iosr.model.SimpleBook;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface AsyncService {
    public CompletableFuture<List<Book>> getBooks(List<SimpleBook> list) throws InterruptedException;
}

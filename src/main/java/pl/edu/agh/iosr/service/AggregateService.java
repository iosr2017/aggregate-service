package pl.edu.agh.iosr.service;

import pl.edu.agh.iosr.model.Book;
import pl.edu.agh.iosr.model.Data;
import pl.edu.agh.iosr.model.SimpleBook;
import pl.edu.agh.iosr.model.Title;

import java.util.List;

public interface AggregateService {
    Data getBasicData(String name);

    List<Book> getBooksFromYear(String value, String quantity) throws InterruptedException;

    List<Book> getBooksAllFromYear(String value) throws InterruptedException;

    Data getBasicDataPost(Title input);

    List<Data> getBasicDataBatch(List<Title> list);
}

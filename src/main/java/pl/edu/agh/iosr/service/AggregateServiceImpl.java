package pl.edu.agh.iosr.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import pl.edu.agh.iosr.client.CrawlerClient;
import pl.edu.agh.iosr.client.LubimyCzytacClient;
import pl.edu.agh.iosr.model.*;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class AggregateServiceImpl implements AggregateService {

    @Autowired
    private LubimyCzytacClient lubimyCzytacClient;
    @Autowired
    private CrawlerClient crawlerClient;
    @Autowired
    private AsyncService asyncService;

    private static final Logger LOG = LoggerFactory.getLogger(AggregateService.class);

    @Override
    @HystrixCommand(fallbackMethod = "fallbackGetBasicData")
    public Data getBasicData(String name) {
        LOG.info("get Basic Data: " + name);
        return lubimyCzytacClient.getBasicDataPost(new Title(name));

    }

    @Override
    @HystrixCommand(fallbackMethod = "fallbackGetBasicDataPost")
    public Data getBasicDataPost(Title input) {
        LOG.info("get Basic Data Post: " + input);
        return lubimyCzytacClient.getBasicDataPost(input);
    }

    @Override
    @HystrixCommand(fallbackMethod = "fallbackGetBasicDataBatch")
    public List<Data> getBasicDataBatch(List<Title> list) {
        LOG.info("get Basic Data Batch: " + list);
        return lubimyCzytacClient.getBasicDataBatch(list);
    }

    @Override
    @HystrixCommand(fallbackMethod = "fallbackGetBooksFromYear")
    public List<Book> getBooksFromYear(String value, String quantity) throws InterruptedException {
        LOG.info("get Books From Year: " + value + " Quantity: " + quantity);
        List<Book> books = new ArrayList<>();
        String next = "0";
        Integer num = Integer.valueOf(quantity);
        int i = 1;
        if (num > 100)
            i = num / 100;

        List<CompletableFuture<List<Book>>> list =  new ArrayList<>();
        while(!next.equals("END") && (i > 0 || num < 0)){
            BookPortion portion = crawlerClient.getBooksFromYear(value,next);
            next = portion.getNextId();
            CompletableFuture<List<Book>> elem = asyncService.getBooks(portion.getBookList());
            list.add(elem);
            i--;
        }

        CompletableFuture.allOf(list.toArray(new CompletableFuture[list.size()])).join();
        list.forEach(c->{
            try {
                books.addAll(c.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });
        return books;
    }



    @Override
    @HystrixCommand(fallbackMethod = "fallbackGetBooksAllFromYear")
    public List<Book> getBooksAllFromYear(String value) throws InterruptedException {
        return getBooksFromYear(value,"-1");
    }


    @HystrixCommand
    public Data fallbackGetBasicData(String name) {
        Data result = new Data("Something went really wrong :(");
        LOG.info("fallback Get Basic Data " + name);
        return result;
    }

    @HystrixCommand
    public Data fallbackGetBasicDataPost(Title input) {
        Data result = new Data("Something went really wrong :(");
        LOG.info("fallback Get Basic Data Post" + input);
        return result;
    }

    @HystrixCommand
    public List<Data> fallbackGetBasicDataBatch(List<Title> list) {
        ArrayList<Data> result = new ArrayList<>();
        result.add(new Data("Something went really wrong :("));
        LOG.info("fallback Get Basic Data Batch" + list);
        return result;
    }

    @HystrixCommand
    public List<Book> fallbackGetBooksFromYear(String value, String quantity) {
        ArrayList<Book> result = new ArrayList<>();
        result.add(new Book(new Data("Something went really wrong :("),new SimpleBook()));
        LOG.info("fallback Get Books From Year " + value + " quantity " + quantity);
        return result;
    }

    @HystrixCommand
    public List<Book> fallbackGetBooksAllFromYear(String value) {
        ArrayList<Book> result = new ArrayList<>();
        result.add(new Book(new Data("Something went really wrong :("),new SimpleBook()));
        LOG.info("fallback Get Books All From Year " + value );
        return result;
    }

}

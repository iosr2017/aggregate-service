package pl.edu.agh.iosr.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.edu.agh.iosr.model.BookPortion;

import java.util.List;

@FeignClient(name = "crawler-service")
public interface CrawlerClient {

        @RequestMapping(method = RequestMethod.GET, value = "/year:{value}/{since}")
        BookPortion getBooksFromYear(@PathVariable("value") String value,@PathVariable("since") String since);

}


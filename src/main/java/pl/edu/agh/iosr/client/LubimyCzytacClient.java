package pl.edu.agh.iosr.client;


import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.agh.iosr.model.Data;
import pl.edu.agh.iosr.model.Title;

import java.util.List;

@FeignClient(name = "lubimy-czytac-service")
public interface LubimyCzytacClient {

    @RequestMapping(method = RequestMethod.GET, value = "/getBasicData/{name}")
    Data getBasicData(@PathVariable("name") String name);

    @RequestMapping(method = RequestMethod.POST, value = "/getBasicData")
    Data getBasicDataPost(Title title);

    @RequestMapping(method = RequestMethod.POST, value = "/getBasicDataBatch")
    List<Data> getBasicDataBatch(List<Title> list);

}

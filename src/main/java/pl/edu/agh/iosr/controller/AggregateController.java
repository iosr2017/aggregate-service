package pl.edu.agh.iosr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.iosr.model.Book;
import pl.edu.agh.iosr.model.Data;
import pl.edu.agh.iosr.model.SimpleBook;
import pl.edu.agh.iosr.model.Title;
import pl.edu.agh.iosr.service.AggregateService;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.List;
@RefreshScope
@RestController
public class AggregateController {
    @Autowired
    private AggregateService aggregateService;

    @RequestMapping(value = "/getBasicData/{name}", method = RequestMethod.GET)
    public Data getBasicData(@PathVariable("name") String name) {
        return aggregateService.getBasicData(name);
    }


    @RequestMapping(value = "/getBasicData", method = RequestMethod.POST)
    public Data getBasicDataPost(@RequestBody Title input) {
        return aggregateService.getBasicDataPost(input);
    }

    @RequestMapping(value = "/getBasicDataBatch", method = RequestMethod.POST)
    public List<Data> getBasicDataBatch(@RequestBody List<Title> input) {
        return aggregateService.getBasicDataBatch(input);
    }

//    @RequestMapping(value = "/getBooksFromYear/{value}", method = RequestMethod.GET)
//    public List<SimpleBook> getBooksFromYear(@PathVariable("value") String value) {
//        return aggregateService.getBooksFromYear(value);
//    }

    @Cacheable("books")
    @RequestMapping(value = "/getBooksAllFromYear/{value}", method = RequestMethod.GET)
    public List<Book> getBooksAllFromYear(@PathVariable("value") String value) throws InterruptedException {
        return aggregateService.getBooksAllFromYear(value);
    }

    @Cacheable("books")
    @RequestMapping(value = "/getBooksFromYear/{value}/{quantity}", method = RequestMethod.GET)
    public List<Book> getBooksFromYear(@PathVariable("value") String value, @PathVariable("quantity") String quantity) throws InterruptedException {
        return aggregateService.getBooksFromYear(value,quantity);
    }


}
